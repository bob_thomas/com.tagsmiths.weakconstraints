<?xml version="1.0" encoding="utf-8" ?>
<!--
This file is part of the DITA Open Toolkit project.

Copyright 2004, 2005 IBM Corporation

See the accompanying LICENSE file for applicable license.
-->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:conref="http://dita-ot.sourceforge.net/ns/200704/conref"
  xmlns:ditamsg="http://dita-ot.sourceforge.net/ns/200704/ditamsg"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:dita-ot="http://dita-ot.sourceforge.net/ns/201007/dita-ot"
  exclude-result-prefixes="ditamsg conref xs dita-ot">

  <xsl:template name="checkValid" as="xs:boolean">
    <xsl:param name="sourceDomains"/>
    <xsl:param name="targetDomains"/>

    <!-- format the out -->
    <xsl:variable name="output" as="xs:string">
      <xsl:variable name="out" as="xs:string">
        <xsl:value-of>
          <xsl:for-each
            select="tokenize($sourceDomains, '\)\s*?')[not(starts-with(normalize-space(.), 'a'))]">
            <xsl:value-of select="concat(., ') ')"/>
          </xsl:for-each>
        </xsl:value-of>
      </xsl:variable>
      <xsl:value-of select="normalize-space($out)"/>
    </xsl:variable>

    <!-- break string into node-set -->
    <xsl:variable name="subDomains" select="reverse(tokenize($output, '\(|\)\s*?\(|\)'))"/>
    <!-- get domains value having constraints e.g [topic simpleSection-c]-->
    <xsl:variable name="constraints" select="$subDomains[contains(., '-c')]"/>
    <xsl:choose>
      <!-- no more constraints -->
      <xsl:when test="empty($constraints)">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <!--get first item in the constraints node set-->
        <xsl:variable name="compareItem" select="$constraints[position() = 1]"/>
        <!-- format the item -->
        <!--e.g (topic hi-d basicHighlight-c)-->
        <xsl:variable name="constraintItem" select="concat('(', $compareItem, ')')"/>
        <!--find out what the original module is. e.g topic, hi-d-->
        <xsl:variable name="originalItem"
          select="tokenize($compareItem, ' ')[not(contains(., '-c'))]"/>
        <!-- if $compareItem is (topic shortdescReq-c task shortdescTaskReq-c), 
             we should remove the compatible values:shortdescReq-c-->
        <xsl:variable name="lastConstraint"
          select="tokenize($compareItem, ' ')[contains(., '-c')][position() = last()]"/>
        <!-- cast sequence to string for compare -->
        <xsl:variable name="module" select="normalize-space(string-join($originalItem, ' '))"
          as="xs:string"/>

        <!-- format the string topic hi-d remove tail space to (topic hi-d) -->
        <xsl:variable name="originalModule" select="concat('(', $module, ')')"/>
        <!--remove compatible constraints-->
        <xsl:variable name="editedConstraintItem"
          select="concat('(', $module, ' ', $lastConstraint)"/>
        <xsl:choose>
          <!-- If the target has constraint item (topic hi-d basicHighlight-c) and there is only one constraint mode left-->
          <!-- If the target has a value that begins with constraint item and there is only one constraint mode left-->
          <xsl:when
            test="
              (contains($targetDomains, $constraintItem) and count($constraints) = 1)
              or (contains($targetDomains, $editedConstraintItem) and count($constraints) = 1)">
            <xsl:sequence select="true()"/>
          </xsl:when>
          <xsl:when
            test="
              count($constraints) > 1 and (contains($targetDomains, $constraintItem) or
              contains($targetDomains, $editedConstraintItem))">
            <xsl:variable name="remainString" as="xs:string">
              <xsl:value-of>
                <xsl:for-each select="remove($constraints, 1)">
                  <xsl:value-of select="concat('s(', ., ')', ' ')"/>
                </xsl:for-each>
              </xsl:value-of>
            </xsl:variable>
            <xsl:call-template name="checkValid">
              <xsl:with-param name="sourceDomains" select="normalize-space($remainString)"/>
              <xsl:with-param name="targetDomains" select="$targetDomains"/>
            </xsl:call-template>
          </xsl:when>
          <!--If the target does not have (topic hi-d) and (topic hi-d, continue to test #2-->
          <xsl:when
            test="not(contains($targetDomains, $originalModule)) and not(contains($targetDomains, substring-before($originalModule, ')')))">
            <xsl:variable name="remainString" as="xs:string">
              <xsl:value-of>
                <xsl:for-each select="remove($constraints, 1)">
                  <xsl:value-of select="concat('s(', ., ')', ' ')"/>
                </xsl:for-each>
              </xsl:value-of>
            </xsl:variable>
            <xsl:call-template name="checkValid">
              <xsl:with-param name="sourceDomains" select="normalize-space($remainString)"/>
              <xsl:with-param name="targetDomains" select="$targetDomains"/>
            </xsl:call-template>
          </xsl:when>
          <!--If the target topic has the original module (topic hi-d) but does not have constraintItem (topic hi-d basicHighlight-c)
              or If the target topic has the beginning original module (topic hi-d but does not have constraintItem (topic hi-d basicHighlight-c)-->
          <!--conref is not allowed  -->
          <xsl:when
            test="
              (contains($targetDomains, $originalModule) and not(contains($targetDomains, $constraintItem)))
              or (contains($targetDomains, substring-before($originalModule, ')')) and not(contains($targetDomains, $constraintItem)))">
            
            <!-- Tagsmiths: Force weak constraints by returning true instead of false. 05dec12-->
            <!--<xsl:sequence select="false()"/>-->
            <xsl:sequence select="true()"/>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
